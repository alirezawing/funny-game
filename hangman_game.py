#!/usr/bin/env python3
"""

print("your welcome")
def main():
    file = open("dfg.txt", 'r')
    if file.mode == 'r':
        x = file.read()
        print(x)
    
    file.close()

if __name__ == "__main__":
    main()
"""
import pygame
import time
import random

pygame.init()

dis_width = 800
dis_height = 600

dis = pygame.display.set_mode((dis_width,dis_height))

pygame.display.set_caption('Snake game by giro')

white = (255, 255, 255)
black = (0,0,0)
blue = (0,0,255)
red = (255,0,0)



snake_block = 10


clock = pygame.time.Clock()
snake_speed = 30
 
font_style = pygame.font.SysFont(None,30)

def message(msg, color):
    mesg = font_style.render(msg, True, color)
    dis.blit(mesg, [dis_width/2, dis_height/2])

def gameloop():
    game_over = False
    game_close = False

    x1 = dis_width/2
    y1 = dis_height/2

    x1_change = 0
    y1_change = 0

    foodx = round(random.randrange(0,dis_width-snake_block)/10.0)*10.0
    foody = round(random.randrange(0, dis_width-snake_block)/10.0)*10.0
    while not game_over:
        while game_close == True:
            dis.file(white)
            message("you lost press q quit or c replay", red)
            pygame.display.update()

            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        game_over = True
                        game_close = False
                    if event.key == pygame.K_c:
                        gameloop()


        for event in pygame.event.get():
            #quit activate
            if event.type == pygame.QUIT:
                game_over = True
            #event activate
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x1_change = -snake_block
                    y1_change = 0
                elif event.key == pygame.K_RIGHT:
                    x1_change = snake_block
                    y1_change = 0
                elif event.key == pygame.K_UP:
                    x1_change = 0
                    y1_change = -snake_block
                elif event.key == pygame.K_DOWN:
                    x1_change = 0
                    y1_change = snake_block
        #boundry conditin
        if x1 >= dis_width or x1 < 0 or y1 >= dis_height or y1 < 0 :
            game_over = True
        x1 += x1_change
        y1 += y1_change
        dis.fill(white)
        #food draw
        pygame.draw.rect(dis, blue, [foodx, foody, snake_block, snake_block])
        #snake draw
        pygame.draw.rect(dis,black,[x1,y1,snake_block,snake_block])
        pygame.display.update()

        if x1 == foodx and y1 == foody:
            print("Yummy!!")
        #speed snake
        clock.tick(snake_speed)

   
    
    pygame.quit()
    quit()

gameloop()